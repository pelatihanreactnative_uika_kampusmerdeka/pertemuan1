class Salam extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    console.log(`Component Salam mounted!`);
  }
  componentDidUpdate(prevProps, prevState, snapshot) {
    console.log("Component Salam Update :", { prevProps, prevState, snapshot });
  }

  render() {
    return (
      <div>
        <center>
          <h2>Hai, {this.props.name}!, selamat datang :)</h2>
          <small>Panjang nama kamu {this.props.name.length} karakter</small>
        </center>
      </div>
    );
  }
}
class FormSalam extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      tampilSalam: false,
    };
    this.arrowFuncChangeName = this.arrowFuncChangeName.bind(this);
    this.changeName = this.changeName.bind(this);
  }

  componentDidMount() {
    console.log(`Component FormSalam mounted!`);
  }
  componentDidUpdate(prevProps, prevState, snapshot) {
    console.log("Component FormSalam Update :", {
      prevProps,
      prevState,
      snapshot,
    });
  }

  changeName(e) {
    this.setState({ name: e.target.value });
  }

  arrowFuncChangeName = (e) => this.changeName(e);

  render() {
    return (
      <div>
        <form>
          <input
            type="text"
            onChange={this.arrowFuncChangeName}
            placeholder="Masukan Nama"
          />
          <br />
          <input
            type="checkbox"
            checked={this.state.tampilSalam}
            onChange={(e) => {
              this.setState({ tampilSalam: !this.state.tampilSalam });
            }}
          />{" "}
          Tampil Salam
          {this.state.name.length > 0 && this.state.tampilSalam && (
            <Salam name={this.state.name} />
          )}
        </form>
      </div>
    );
  }
}
//quiz
//buat input bilangan A dan B
//buat kalkulasi tambah,kurang,bagi,modulus,dan A pangkat B
//inisiasi nilai A:7 nilai B:6, tidak boleh inisiasi d construtor HUT RI 76
//hadiah 25rb OVO/DANA untuk 2 peserta yg bruntung
//waktu pengerjaan 30 menit
//Wajib buat 2 komponen 1 komponen input & 1 komponen hasil/ouput

class QuizHasilPerhitungan extends React.Component {
  constructor(props) {
    super(props);
  }
  Pertambahan = (a, b) => a + b;
  Pengurangan = (a, b) => a - b;
  Pembagian = (a, b) => a / b;
  Modulus = (a, b) => a % b;
  Pangkat = (a, b) => {
    let value = 1;
    for (let i = 0; i < b; i++) {
      value *= a;
    }
    return value;
  };

  render() {
    return (
      <div>
        <hr />
        Hasil {this.props.bilanganA}+{this.props.bilanganB}=
        {this.Pertambahan(this.props.bilanganA, this.props.bilanganB)}
        <br />
        Hasil {this.props.bilanganA}-{this.props.bilanganB}=
        {this.Pengurangan(this.props.bilanganA, this.props.bilanganB)}
        <br />
        Hasil {this.props.bilanganA}/{this.props.bilanganB}=
        {this.Pembagian(this.props.bilanganA, this.props.bilanganB)}
        <br />
        Hasil {this.props.bilanganA}%{this.props.bilanganB}=
        {this.Modulus(this.props.bilanganA, this.props.bilanganB)}
        <br />
        Hasil {this.props.bilanganA}^{this.props.bilanganB}=
        {this.Pangkat(this.props.bilanganA, this.props.bilanganB)}
      </div>
    );
  }
}
class Quiz extends React.Component {
  state = {
    bilanganA: 0,
    bilanganB: 0,
  };
  constructor(props) {
    super(props);
    this.onChangeInput = this.onChangeInput.bind(this);
  }

  componentDidMount() {
    this.setState({
      bilanganA: 7,
      bilanganB: 6,
    });
  }
  onChangeInput(e) {
    this.setState({ [e.target.id]: e.target.value });
  }

  render() {
    return (
      <div>
        <hr />
        Quiz
        <br />
        Bilangan A:{" "}
        <input
          id="bilanganA"
          type="number"
          value={this.state.bilanganA}
          onChange={this.onChangeInput}
        />
        <br />
        Bilangan B:{" "}
        <input
          id="bilanganB"
          type="number"
          value={this.state.bilanganB}
          onChange={this.onChangeInput}
        />
        <QuizHasilPerhitungan
          bilanganA={Number(this.state.bilanganA)}
          bilanganB={Number(this.state.bilanganB)}
        />
      </div>
    );
  }
}
class App extends React.Component {
  render() {
    return (
      <div>
        <center>
          <h3>JSX, ES dan React dalam HTML</h3>
          <b>Masukan Nama Kamu</b>
          <FormSalam />
          <Quiz />
        </center>
      </div>
    );
  }
}
const root = document.querySelector("#root");
ReactDOM.render(<App />, root);
