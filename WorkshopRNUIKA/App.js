import React, {Component} from 'react';
import {View, Text, TextInput} from 'react-native';
class Salam extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View style={{alignItems: 'center'}}>
        <Text>Hai, {this.props.name}!, selamat datang :)</Text>
        <Text>Panjang nama kamu {this.props.name.length} karakter</Text>
      </View>
    );
  }
}
class FormSalam extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
    };
    this.changeName = this.changeName.bind(this);
  }

  changeName(e) {
    this.setState({name: e.nativeEvent.text});
  }

  render() {
    return (
      <View style={{alignItems: 'center'}}>
        <TextInput
          keyboardType="default"
          onChange={this.changeName}
          placeholder="Masukan Nama"
        />
        {this.state.name.length > 0 && <Salam name={this.state.name} />}
      </View>
    );
  }
}
//quiz
//buat input bilangan A dan B dan C
//buat kalkulasi tambah(a+b+c),kurang(a-b-c),bagi(a/b/c),persamaan distributif(a+b)c, perhitungan rumus (a^c)*b
//inisiasi nilai A:7 nilai B:6,nilai C:8, tidak boleh inisiasi d construtor HUT RI 76
//hadiah 25rb OVO/DANA untuk 2 peserta yg bruntung
//waktu pengerjaan 30 menit
//Wajib buat 2 komponen 1 komponen input & 1 komponen hasil/ouput

class QuizHasilPerhitungan extends Component {
  constructor(props) {
    super(props);
  }
  Pertambahan = (a, b, c) => a + b + c;
  Pengurangan = (a, b, c) => a - b - c;
  Pembagian = (a, b, c) => a / b / c;
  PersamaanDistributif = (a, b, c) => a * c + b * c;
  HitungRumus = (a, b, c) => {
    let value = 1;
    for (let i = 0; i < c; i++) {
      value *= a;
    }
    return value * b;
  };

  render() {
    return (
      <View style={{alignItems: 'center'}}>
        <Text>
          Hasil {this.props.bilanganA}+{this.props.bilanganB}+
          {this.props.bilanganC}=
          {this.Pertambahan(
            this.props.bilanganA,
            this.props.bilanganB,
            this.props.bilanganC,
          )}
        </Text>
        <Text>
          Hasil {this.props.bilanganA}-{this.props.bilanganB}-
          {this.props.bilanganC}=
          {this.Pengurangan(
            this.props.bilanganA,
            this.props.bilanganB,
            this.props.bilanganC,
          )}
        </Text>
        <Text>
          Hasil {this.props.bilanganA}/{this.props.bilanganB}/
          {this.props.bilanganC}=
          {this.Pembagian(
            this.props.bilanganA,
            this.props.bilanganB,
            this.props.bilanganC,
          )}
        </Text>
        <Text>
          Hasil ({this.props.bilanganA}+{this.props.bilanganB})*
          {this.props.bilanganC}=
          {this.PersamaanDistributif(
            this.props.bilanganA,
            this.props.bilanganB,
            this.props.bilanganC,
          )}
        </Text>
        <Text>
          Hasil ({this.props.bilanganA}^{this.props.bilanganC}) *
          {this.props.bilanganB}=
          {this.HitungRumus(
            this.props.bilanganA,
            this.props.bilanganB,
            this.props.bilanganC,
          )}
        </Text>
      </View>
    );
  }
}
class Quiz extends Component {
  state = {
    bilanganA: '0',
    bilanganB: '0',
    bilanganC: '0',
  };
  constructor(props) {
    super(props);
    this.onChangeInput = this.onChangeInput.bind(this);
  }

  componentDidMount() {
    this.setState({
      bilanganA: '7',
      bilanganB: '6',
      bilanganC: '8',
    });
  }
  onChangeInput(key, val) {
    this.setState({[key]: val});
  }

  render() {
    return (
      <View style={{alignItems: 'center'}}>
        <Text>Quiz</Text>
        <Text>Bilangan A: </Text>
        <TextInput
          keyboardType="numeric"
          placeholder="Masukan Nilai A"
          value={this.state.bilanganA}
          onChangeText={val => this.onChangeInput('bilanganA', val)}
        />
        <Text>Bilangan B: </Text>
        <TextInput
          keyboardType="numeric"
          placeholder="Masukan Nilai B"
          value={this.state.bilanganB}
          onChangeText={val => this.onChangeInput('bilanganB', val)}
        />
        <Text>Bilangan C: </Text>
        <TextInput
          keyboardType="numeric"
          placeholder="Masukan Nilai C"
          value={this.state.bilanganC}
          onChangeText={val => this.onChangeInput('bilanganC', val)}
        />
        <QuizHasilPerhitungan
          bilanganA={Number(this.state.bilanganA)}
          bilanganB={Number(this.state.bilanganB)}
          bilanganC={Number(this.state.bilanganC)}
        />
      </View>
    );
  }
}
class App extends Component {
  render() {
    return (
      <View style={{alignItems: 'center'}}>
        <Text>Hello World!</Text>
        <Text>Masukan Nama Kamu</Text>
        <FormSalam />
        <Quiz />
      </View>
    );
  }
}

export default App;
